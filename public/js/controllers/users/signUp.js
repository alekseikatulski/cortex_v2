angular.module('mean.auth').controller('signUp', ['$scope', '$window', 'Global', '$state', 'SignUp', function($scope, $window, Global, $state, SignUp) {
    $scope.global = Global;
    $scope.signUp = function(user) {
        $scope.changedValue = function(item) {
            user.type = item
        }
        var signUp = new SignUp({
            name: user.name,
            email: user.email,
            username: user.username,
            password: user.password,
            phone: user.phone,
            type: user.type,
            status: user.status
        });

        signUp.$save(function(response) {
            if (response.status === 'success') {
                $window.location.href = '/';
            }
        });
    };
}]);